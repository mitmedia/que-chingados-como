var mongoskin = require('mongoskin');
var id = mongoskin.helper.toObjectID;

exports.get = function(req, res, next) {
	console.log('api: place-get');

	req.collection.place.count(function(err, count){
		var skip = Math.floor((Math.random() * count));
		console.log('skip: ' + skip);
		req.collection.place.find({}, null, {limit:1, skip: skip}).toArray(function(error, results){
			if (error) {
				return res.send({
					status: false,
					msg: error
				});
			}
			return res.send({
				status: true,
				data: results[0]
			});
		})
	});
};

exports.add = function(req, res, next) {
	console.log('api: place-add');
	if (!req.body.hasOwnProperty('place')) {
		return res.send({
			status: false,
			msg: 'No place payload.'
		});
	}

	var place = req.body.place;
	var fields = ['name', 'description', 'type', 'price', 'image', 'address', 'facebook', 'telephone', 'has_delivery'];
	var missed_fields = [];
	for (var i = 0; i < fields.length; i++) {
		if (!place.hasOwnProperty(fields[i]) || !place[fields[i]]) {
			missed_fields.push(fields[i]);
		}
	}

	if (missed_fields.length) {
		return res.send({
			status: false,
			msg: 'All fields are required. ' + missed_fields.join('|')
		});
	}

	place.published = false;
	req.collection.place.insert(place, function(error, response) {
		if (error) {
			return next(error);
		}
		return res.send({
			status: true,
			data: response
		});
	});
};