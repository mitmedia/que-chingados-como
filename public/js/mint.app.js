var mintApp = angular.module('mintApp', ['ngRoute']);

mintApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.
    when('/', {
        templateUrl: '/partial/home',
        controller: 'mainCtrl'
    }).
    when('/nosotros', {
        templateUrl: '/partial/about-us'
    }).
    when('/lugar/agregar', {
        templateUrl: '/place/add',
        controller: 'placeCtrl'
    }).
    otherwise({
        redirectTo: '/'
    });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);

mintApp.controller('gralCtrl', ['$scope', function($scope) {

    var init = function() {
        console.log('gralCtrl init');
    };

    init();
}]);

mintApp.controller('mainCtrl', ['$rootScope', '$scope', '$http', '$q', function($rootScope, $scope, $http, $q) {

    $scope.getPlace = function() {
        var defer = $q.defer();
        var promise = defer.promise;
        var priceLabel = ['', 'Bien bara', 'Dos triquis', 'Ponte guapo'];

        $http.get('api/place', {
            'id': 1
        }).then(
            function(res) {
                defer.resolve(res.data);
            },
            function(err) {
                defer.reject(err.data);
            });
        promise.then(function(data) {
            if (data.hasOwnProperty('status') && data.status) {
                if (data.hasOwnProperty('data') && angular.isObject(data.data)) {
                    $scope.place = data.data;
                    $scope.place.price = priceLabel[$scope.place.price];
                    $scope.setPhrase();
                    setContentMinHeight();
                }
            } else {
                console.log('error');
            }
        });
    };

    $scope.setPhrase = function() {
        $scope.phrase = phraseList[parseInt(Math.random(1) * phraseList.length)];
    };

    var phraseList = [
        "Manda al que llego tarde hoy por la comida",
        "De peores lugares me han corrido, mejor voy a",
        "Pa ' que no digas que nunca te saco, te voy a llevar a",
        "Calentar en el 'micro' te hace dano, hoy ve a",
        "Si quieres quedar bien llevalos a comer a",
        "Ni que te pagaran horas extra, hoy sal a comer a",
        "Dile al jefe que ahorita regresas que vas a comer a",
        "¿Para que cocinas? cuando puedes ir a",
        "Para 'lonche' en la primaria dejate caer a",
        "Mañana te haces un sandwich hoy ve a"
    ];

    var init = function() {
        console.log('mainCtrl init');
        $rootScope.isHome = true;
        $scope.getPlace();
        $scope.setPhrase();
    };

    init();
}]);

mintApp.controller('placeCtrl', ['$rootScope', '$scope', '$http', '$q', function($rootScope, $scope, $http, $q) {

    $scope.addPlace = function(form, place) {
        $scope.validateForm = true;
        $scope.msg = '';

        if (form && form.$valid && place) {
            var defer = $q.defer();
            var promise = defer.promise;
            $scope.msg = 'sending...';
            if (!place.image) {
                place.image = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=650%C3%97550&w=650&h=550';
            }
            $http.post('/api/place', {
                'place': place
            }).then(
                function(res) {
                    defer.resolve(res.data);
                },
                function(err) {
                    defer.reject(err.data);
                });
            promise.then(function(data) {
                if (data.status) {
                    $scope.msg = 'saved';
                    $scope.place = initPlace();
                    form.$setPristine();
                    form.$setUntouched();
                    $scope.validateForm = false;
                } else {
                    $scope.msg = 'errors';
                }
            });
        } else {
            $scope.msg = 'Hay algunos errores en la forma, por favor corríjalos e intente de nuevo';
        }
    };

    $scope.init = function() {
        console.log('placeCtrl init');
        $scope.place = initPlace();
        $scope.addPlaceForm
        $rootScope.isHome = false;
    };

    $scope.init();
}]);

var initPlace = function() {
    return {
        has_delivery: 'true',
        price: 2
    };
};

var setContentMinHeight = function() {
    if (window.innerWidth >= 768) {
        var header = document.getElementById('header').offsetHeight,
            suggestionHeader = document.getElementById('suggestion-header').offsetHeight,
            suggestionTitle = document.getElementById('suggestion-title').offsetHeight,
            footer = document.getElementById('footer').offsetHeight,
            windowHeight = window.innerHeight,
            minHeight = windowHeight - (header + suggestionHeader + suggestionTitle + footer);
        isFirstTime = false;
        // console.log('header', 'suggestionHeader', 'suggestionTitle', 'footer', 'windowHeight', 'minHeight');
        // console.log(header, suggestionHeader, suggestionTitle, footer, windowHeight, minHeight);
        document.getElementById('suggestion-content').style.minHeight = minHeight + 'px';
        document.getElementById('place-image').style.height = minHeight + 'px';
    }
};
window.addEventListener("resize", setContentMinHeight);
