var express = require('express'),
	http = require('http'),
	path = require('path'),
	routes = require('./routes'),
	api = require('./api'),
	mongoskin = require('mongoskin'),
	dbName = 'qchc',
	dbUrl = process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://@localhost:27017/',
	db = mongoskin.db(dbUrl + dbName, {
		safe: true
	}),
	collection = {
		place: db.collection('place'),
	};
console.log('process.env.OPENSHIFT_MONGODB_DB_URL', process.env.OPENSHIFT_MONGODB_DB_URL);
var session = require('express-session'),
	logger = require('morgan'),
	errorHandler = require('errorhandler'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	methodOverride = require('method-override');

var app = express();

app.use(function(req, res, next) {
	if (!collection.place) return next(new Error("No collections."));
	req.collection = collection;
	return next();
});

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/partial/home', routes.partial.home);
app.get('/partial/about-us', routes.partial.about_us);

app.get('/place/add', routes.place.add);

app.get('/api/place', api.place.get);
app.post('/api/place', api.place.add);

app.get('/', routes.home);
app.get('/lugar/agregar', routes.home);

app.all('*', function(req, res) {
	res.send(404);
});

http.createServer(app).listen(app.get('port'), function() {
	console.info('Express server listening on port ' + app.get('port'));
});