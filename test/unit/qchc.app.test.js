var expect = chai.expect;
var assert = chai.assert;
var should = chai.should;

describe('qchc.app.mainCtrl', function() {
	var scope;
	var ctrl;
	var deferred;

	// beforeEach(module('ui.bootstrap'));
	// beforeEach(module('nsc.app.modalService'));
	beforeEach(module('ngRoute'));
	// beforeEach(module('nsc.app.cacheFactory'));
	// beforeEach(module('nsc.app.constants'));
	// beforeEach(module('nsc.app.contextService'));
	// beforeEach(module('nsc.app.contextSelector'));
	// beforeEach(module('nsc.app.accountService'));
	// beforeEach(module('nsc.app.productService'));
	// beforeEach(module('nsc.app.productProductsController'));

	beforeEach(inject(function($rootScope, $controller) {
		scope = $rootScope.$new();
		ctrl = $controller('mainCtrl', {
			$scope: scope
		});
	}));

	describe('Browse', function($q) {
		expect(1).to.equal(1);
		// var postFn = sinon.spy(function() {
		// 	return deferred.resolve();
		// });

		// beforeEach(inject(function($q) {
		// 	sinon.stub(ProductService, 'getProducts', function() {
		// 		scope.products = [{
		// 			code: 12493,
		// 			name: 'Stuffed Crust Pizza',
		// 		}];
		// 		return postFn();
		// 	});

		// 	sinon.stub(scope, '$nscContextTreeId', function() {
		// 		return 1;
		// 	});

		// 	deferred = $q.defer();
		// }));

		// afterEach(function() {
		// 	ProductService.getProducts.restore();
		// 	scope.$nscContextTreeId.restore();
		// 	postFn.reset();
		// });

		// it('should be 8 columns', function() {
		// 	expect(scope.columns.length).to.equal(8);
		// });

		// it('should call $nscContextTreeId ang get a valid value', function() {
		// 	expect(scope.$nscContextTreeId()).to.equal(1);
		// });

		// it('should resolve a promise on ProductService.getProducts', function() {
		// 	var promise = deferred.promise;

		// 	ProductService.getProducts(scope.$nscContextTreeId());

		// 	/*jshint -W030 */
		// 	expect(ProductService.getProducts.calledOnce).to.be.true;
		// 	expect(postFn.calledOnce).to.be.true;
		// 	expect(ProductService.getProducts.calledWithMatch(1)).to.be.true;
		// 	/* jshint +W030 */

		// 	expect(scope.products.length).to.equal(1);
		// });
	});
});