Project: ¿Qué chingados como?

Description:
A website that suggests random places to eat, with a little bit of foul language.
This website is a collaboration between Pool Branding (http://somospool.com/) and Mint IT Media (http://mintitmedia.com/).

Technologies:

    Node JS
    Mongo DB

How to get your project running

    Download and install Node JS (https://nodejs.org/download/)
    Download and install Mongo DB (http://docs.mongodb.org/manual/installation/)
    Get your Database running

    Check if you installed correctly mongo DB. In your terminal write $ which mongo, it should give you a path that points to your mongo installation.

    Move to the bin directory that contains mongo DB. Check the path you got, you should have something like /usr/local/bin/mongo, change to the directory that contains mongo (meaning, don't use the whole path), for example, in this case it should be $ cd /usr/local/bin in your case $ cd /your/path/to/bin.

    Populate the database (first time only). If it's your first time running the project, you need to populate the database. In a different tab move to the root of the project. Use $ resources/data to change to the data directory, then type $ mongoimport --db qchc --collection place --file db.json. When you run the project you'll have to registered places. We are done with that tab.

4.Run the database. If you populated the database for the first time return to the tab with the bin directory. Just type $ ./mongod, that's it.

    Start your server. Leave the DB tab alone and open a new tab in the terminal.
    Install node's libraries (first time only). Write $npm install in your terminal.
    Start the server. Type $ npm start in your terminal.

That's it. To stop mongo or your server use Command + c or Ctrl + c in the relevant terminal tab.


Summary:

* Clone project
* move inside project
* Checkout dev branch
* Run mongo `mongod`
* Install db (only first time)
* Run `npm start`
* Wait for "Express server listening on port XXX" message and then open your browser on that port.
* Before doing any change make sure you have a new branch (topics/branch-name)


Database

$ mongod
$ mongoimport --db qchc --collection place --file resources/data/db.json